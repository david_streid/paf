import unittest
from process_sv_vcf import get_match_proportion, MATCH_INTERVAL_START_IDX, Comparison, identify_same_samples

def get_entry(chr, pos, sv_type, sv_len):
  match_interval_start = pos
  match_interval_end = pos + sv_len
  return [ chr, sv_type, pos, match_interval_start, match_interval_end ]

class TestStructuralVariationAnnotator(unittest.TestCase):
  def test_identify_same_samples(self):
    s1_1 = 's1_1'   # 1
    s1_2 = 's1_2'   # 2
    s1_3 = 's1_3'   # 3
    s1_4 = 's1_4'   # 4
    s1_5 = 's1_5'   # 5
    s2_1 = 's2_1'   # 6
    s2_2 = 's2_2'   # 7
    s3_1 = 's3_1'   # 8
    s3_2 = 's3_2'   # 9
    s3_3 = 's3_3'   # 10
    s3_4 = 's3_4'   # 11
    s3_5 = 's3_5'   # 12
    s3_6 = 's3_6'   # 13
    s4_1 = 's4_1'   # 14
    s4_2 = 's4_2'   # 15
    
    prop = 1.0
    is_match = True

    c1 = Comparison(s1_1, s1_2, prop, is_match)
    c2 = Comparison(s1_2, s1_3, prop, is_match)
    c3 = Comparison(s1_2, s1_4, prop, is_match)
    c4 = Comparison(s1_4, s1_5, prop, is_match)

    c5 = Comparison(s2_1, s2_2, prop, is_match)

    c6 = Comparison(s3_1, s3_2, prop, is_match)
    c7 = Comparison(s3_1, s3_3, prop, is_match)
    c8 = Comparison(s3_1, s3_4, prop, is_match)
    c9 = Comparison(s3_1, s3_5, prop, is_match)
    c10 = Comparison(s3_1, s3_6, prop, is_match)

    c11 = Comparison(s4_1, s4_2, prop, is_match)

    c_list = [ c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11 ]

    sample_num, samples, sample_to_bioruns_list, all_matched_bioruns_set = identify_same_samples(c_list)

    all_matched_bioruns_list = list(all_matched_bioruns_set)

    self.assertEqual(sample_num, 4)
    self.assertEqual(len(sample_to_bioruns_list), 4)
    self.assertEqual(len(all_matched_bioruns_list), 15)

  def test_get_overlapping_entries(self):
    
    match_entry = get_entry('1', 15, 'DUP', 21)       # 15, 35
    match_entry_2 = get_entry('1', 15, 'DUP', 18)     # 15, 32

    no_match_entry = get_entry('1', 100, 'DUP', 1)    # 100, 100
    no_match_entry_2 = get_entry('1', 101, 'DUP', 1)  # 100, 100
    query_list = sorted([ no_match_entry, match_entry, no_match_entry_2, match_entry_2 ], key=lambda entry: entry[MATCH_INTERVAL_START_IDX])
    expected_match_proportion = 0.5     

    # No Overlap
    n1 = get_entry('1', 5, 'DUP', 10)       # 5, 14
    n2 = get_entry('1', 13, 'DUP', 2)       # 13, 14
    n3 = get_entry('1', 35, 'DUP', 10)      # 35, 44

    # Overlap, not matches
    o1 = get_entry('1', 14, 'DUP', 2)       # 14, 15
    o2 = get_entry('1', 4, 'DUP', 12)       # 4, 15
    o3 = get_entry('1', 15, 'DUP', 15)      # 15, 29
    o4 = get_entry('1', 15, 'DUP', 29)      # 15, 43
    o5 = get_entry('1', 30, 'DUP', 10)      # 30, 49
    o6 = get_entry('1', 34, 'DUP', 12)      # 34, 45

    # Matches
    m1 = get_entry('1', 14, 'DUP', 20)      # 14, 33
    m2 = get_entry('1', 15, 'DUP', 19)      # 15, 33
    
    entry_list = sorted([n1, n2, n3, o1, o2, o3, o4, o5, o6, m1, m2], key=lambda entry: entry[MATCH_INTERVAL_START_IDX])

    v1 = { 'chrs': {'1': query_list }, 'num_vyx_entries': len(query_list) }
    v2 = { 'chrs': {'1': entry_list}, 'num_vyx_entries': len(entry_list) }

    v1_vs_v2_prop = get_match_proportion(v1, v2)
    v2_vs_v1_prop = get_match_proportion(v1, v2)

    self.assertEqual(v1_vs_v2_prop, expected_match_proportion)
    self.assertEqual(v2_vs_v1_prop, expected_match_proportion)


if __name__ == '__main__':
    unittest.main()