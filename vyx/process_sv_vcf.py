import sys
import os
import time
import matplotlib.pyplot as plt

MIN_POS_OVERLAP = 0.9
SINGLE_POSITION_INTERVAL = 20

WARNINGS = {}

SV_LOH = 'LOH'
SV_HERVK = 'HERVK'
SV_DUP = 'DUP'
SV_DEL = 'DEL'
SV_BND = 'BND'
SV_INV = 'INV'
SV_ALU = 'ALU'
SV_LINE1 = 'LINE1'
SV_SVA = 'SVA'

SINGLE_POSITION_EVENTS = { SV_BND }

def graph(num_list, title):
  num_bins = 10
  plt.hist(num_list, num_bins, facecolor='blue', alpha=0.5)
  plt.title(title)
  plt.xlabel('Match Proportion')                    
  plt.ylabel('Count')   
  fname = "_".join(title.split(" "))
  plt.savefig("%s.pdf" % fname)
  plt.close()

def get_reciprocal_position_overlap(pos1, pos2):
  ''' Returns the minimum overlap between two regions 
  :param start1, stop1, start2, stop2; int
  '''
  len1 = get_pos_len(pos1)
  len2 = get_pos_len(pos2)
  overlap_bases = get_overlap_bases(pos1, pos2)

  return min(overlap_bases / len1, overlap_bases / len2) 

def get_overlap_bases(pos1, pos2):
  start1, stop1 = pos1
  start2, stop2 = pos2
  return min(stop1, stop2) - max(start1, start2)

def get_pos_len(pos):
  ''' Unpacks @pos
  :param pos, (int, int)
  '''
  start, stop = pos
  len = stop - start
  return len

class Comparison:
  def __init__(self, biorun_1, biorun_2, match_proportion, is_match):
    self.biorun_1 = biorun_1
    self.biorun_2 = biorun_2
    self.match_proportion = match_proportion
    self.is_match = is_match

def is_lumpy(info_line):
  return 'SR=' in info_line

def is_depth(format_val):
  return 'GT:CPY' in format_val

def include_in_vyx(info_line, format_val):
  # TODO - I think checking if it is lumpy is good enough (it's never lumpy & depth)
  return is_lumpy(info_line) and not is_depth(format_val)   

def get_match_interval(pos, sv_type, end, sv_len):
  '''Returns the interval to use for approximate matching
  :pos, int
  :sv_type, str
  :sv_len, int
  '''
  if sv_type in SINGLE_POSITION_EVENTS:
    # Provide range when VCF position & length are of size 1
    return pos - SINGLE_POSITION_INTERVAL, pos + SINGLE_POSITION_INTERVAL
  if end:
    # If an end is identified, the interval is the SV's start & stop
    # This should be considered BEFORE info's "SVLEN" b/c SVLEN < 0 for deletions
    return pos, int(end)
  if sv_len > 0:
    # If no end is identified, use the length of the structural variant
    return pos, pos + sv_len
  
  # Catch-all, specifically @SV_LINE1 variants which have an info 'SVLEN' < 0 and undefined 'END'
  return pos, pos + 1

def get_info_map(info):
  kv_list = info.split(';')
  map = {}
  for kv in kv_list:
    pair = kv.split('=')
    if len(pair) == 2:
      k = pair[0]
      v = pair[1]
      map[k] = v
  return map

def process_vcf_line(line):
  vals = line.split('\t')
  chr = vals[0]
  pos = int(vals[1])
  info = vals[7]

  info_map = get_info_map(info)
  sv_type = info_map['SVTYPE']
  sv_len = int(info_map.get('SVLEN', 0))
  end = info_map.get('END', None)

  match_interval_start, match_interval_end = get_match_interval(pos, sv_type, end, sv_len)

  return [ chr, sv_type, pos, match_interval_start, match_interval_end ]

CHR_IDX = 0
SV_TYPE_IDX = 1
POS_IDX = 2
MATCH_INTERVAL_START_IDX = 3
MATCH_INTERVAL_END_IDX = 4

def is_pos_match(target_vcf_list, query_vcf_list):
  target_sv_type = target_vcf_list[SV_TYPE_IDX]
  target_pos = target_vcf_list[POS_IDX]
  target_match_interval_start = target_vcf_list[MATCH_INTERVAL_START_IDX]
  target_match_interval_end = target_vcf_list[MATCH_INTERVAL_END_IDX]

  query_sv_type = query_vcf_list[SV_TYPE_IDX]
  query_pos = query_vcf_list[POS_IDX]
  query_match_interval_start = query_vcf_list[MATCH_INTERVAL_START_IDX]
  query_match_interval_end = query_vcf_list[MATCH_INTERVAL_END_IDX]

  if query_sv_type in SINGLE_POSITION_EVENTS and target_sv_type in SINGLE_POSITION_EVENTS:
    return target_match_interval_start < query_pos and target_match_interval_end > query_pos

  target_pos = (target_match_interval_start, target_match_interval_end)
  query_pos = (query_match_interval_start, query_match_interval_end)
  
  overlap = get_reciprocal_position_overlap(target_pos, query_pos)
  return overlap >= MIN_POS_OVERLAP


def is_vcf_list_match(target_vcf_list, query_vcf_list):
  ''' TODO
        - What fields in the INFO are important to match on? Right now, it is just 'SVTYPE'
  '''
  target_chr = target_vcf_list[CHR_IDX]
  target_sv_type = target_vcf_list[SV_TYPE_IDX]

  query_chr = query_vcf_list[CHR_IDX]
  query_sv_type = query_vcf_list[SV_TYPE_IDX]

  return target_chr == query_chr and \
    target_sv_type != None and target_sv_type == query_sv_type and \
    is_pos_match(target_vcf_list, query_vcf_list)


def only_chars(num, num_chars):
  max_reported_val = 1 * (10 ** -(num_chars-2))
  if num < max_reported_val:
    return '<%s' % format(float(max_reported_val),'f')

  num_str = str(num)
  return '=%s' % num_str[0:num_chars]

def summ_stats(num_list):
  if len(num_list) == 0:
    return
  min_of_list = only_chars(min(num_list), 7)
  max_of_list = only_chars(max(num_list), 7)
  len_of_list = len(num_list)
  median_of_list = only_chars(num_list[int(len_of_list/2)], 7)
  avg_of_list = only_chars(sum(num_list)/len_of_list, 7)
  list_size = only_chars(len_of_list, 7)

  print("\tavg%s median%s max%s min%s num%s" % (avg_of_list, median_of_list, max_of_list, min_of_list, list_size))

def get_vcf_map(vcf_fname, biorun):
  total = 0
  added = 0

  chr_map = {}
  vcf_map = { 'biorun': biorun, 'chrs': chr_map }
  with open(vcf_fname, 'r') as vcf_contents:
    for line in vcf_contents:
      if line.startswith('#'):
        continue
      total += 1
      vals = line.split('\t')
      chr = vals[0]
      info = vals[7]
      format = vals[8]

      # TODO - loading into memory is very memory intensive
      # entry = Vcf_Entry(chr, pos, alt, filter, info, format)
      if include_in_vyx(info, format):
        added += 1
        entry = process_vcf_line(line)
        if chr in chr_map:
          chr_map[chr].append(entry)
        else:
          chr_map[chr]= [entry]

  for entry_list in vcf_map['chrs'].values():
    entry_list.sort(key=lambda entry: entry[MATCH_INTERVAL_START_IDX])

  vcf_map['num_vyx_entries'] = added
  vcf_map['num_vcf_entries'] = total

  # print("\tBIORUN=%s ADDED=%s TOTAL=%s" % (biorun, added, total))

  return vcf_map

def get_match_proportion(vcf_map1, vcf_map2):
  chr_map1 = vcf_map1['chrs']
  chr_map2 = vcf_map2['chrs']

  num_vcf_entries1 = vcf_map1['num_vyx_entries']
  num_vcf_entries2 = vcf_map2['num_vyx_entries']

  if num_vcf_entries1 == 0 or num_vcf_entries2 == 0:
    return False

  num_matches = 0
  for chr, entry_list1 in chr_map1.items():
    entry_list2 = chr_map2.get(chr, [])

    if len(entry_list2) == 0:
      type = 'MISSING CHR=%s' % chr
      biorun = vcf_map2['biorun']
      if type in WARNINGS:
        WARNINGS[type].add(biorun)
      else:
        WARNINGS[type] = {biorun}
      continue
    
    q1 = entry_list1.copy()
    q2 = entry_list2.copy()
    while len(q1) > 0 and len(q2) > 0: 
      q1_nxt = q1[0]
      q2_nxt = q2[0]
      q1_indices_overlapping_e2 = get_overlapping_indices(q2_nxt, q1)
      q2_indices_overlapping_e1 = get_overlapping_indices(q1_nxt, q2)

      # No overlapping entries exist
      if len(q1_indices_overlapping_e2) == 0 and len(q2_indices_overlapping_e1) == 0:
        if q1_nxt[MATCH_INTERVAL_START_IDX] < q2_nxt[MATCH_INTERVAL_START_IDX]: # q1_nxt.match_interval_start < q2_nxt.match_interval_start:
          q1.pop(0)
        elif q1_nxt[MATCH_INTERVAL_START_IDX] > q2_nxt[MATCH_INTERVAL_START_IDX]:
          q2.pop(0)
        else:
          q1.pop(0)
          q2.pop(0)
        continue

      match = False
      for i2 in q2_indices_overlapping_e1:
        c2 = q2[i2]
        if is_vcf_list_match(q1_nxt, c2): # if q1_nxt.is_match(c2):
          num_matches += 1
          # matches.append([q1_nxt, c2])
          q1.pop(0)
          q2.pop(i2)
          match = True
          break

      if not match:
        for i1 in q1_indices_overlapping_e2:
          c1 = q1[i1]
          if is_vcf_list_match(q2_nxt, c1):
          # if q2_nxt.is_match(c1):
            num_matches += 1
            # matches.append([c1, q2_nxt])
            match = True
            q1.pop(i1)
            q2.pop(0)
            break

      if not match:
        q1.pop(0)
        q2.pop(0)   

  match_proportion = max(num_matches/num_vcf_entries1, num_matches/num_vcf_entries2)

  return match_proportion

def get_overlapping_indices(entry_query, entry_list):
  ''' Return list of indices of entries in the @entry_list that overlap @entry_query
      - NOTE - assumes entry_list is sorted on match_interval_start
  :param entry_query, Vcf_Entry
  :param entry_list, Vcf_Entry[]
  :return int[]
  '''
  if len(entry_list) == 0:
    return []

  min = entry_query[MATCH_INTERVAL_START_IDX] # .match_interval_start
  max = entry_query[MATCH_INTERVAL_END_IDX]   # .match_interval_end

  # Find any entry in the @entry_list w/ a start position in the interval
  candidate_indices = []
  idx = 0
  nxt = entry_list[idx]

  while nxt and \
    nxt[MATCH_INTERVAL_START_IDX] <= max: # ]match_interval_start <= max:

    if nxt[MATCH_INTERVAL_END_IDX] >= min: # match_interval_end >= min:
      candidate_indices.append(idx)

    idx += 1
    if idx < len(entry_list) - 1:
      nxt = entry_list[idx] 
    else:       
      nxt = None

  return candidate_indices

def get_biorun(path_name):
  return os.path.basename(os.path.dirname(path_name))

def log_comparisons(comparisons_list, out_file):
  with open(out_file, 'w') as out:
    for comp in comparisons_list:
      line = '%s\t%s\t%s\t%s\n' % (comp.biorun_1, comp.biorun_2, comp.match_proportion, comp.is_match)
      out.write(line)

def log_samples(samples_tsv_contents, out_file):
  with open(out_file, 'w') as out:
    for sample in samples_tsv_contents:
      line = '%s\n' % '\t'.join(sample)
      out.write(line)

def identify_same_samples(match_list):
  ''' Identifies shared-samples across bioruns
  @param match_list, Comparison[]
  return int, str[][]
  '''
  graph = {}
  for edge in match_list:
    br1 = edge.biorun_1
    br2 = edge.biorun_2
    if br1 in graph:
      graph[br1].add(edge)
    else:
      graph[br1] = {edge}
    if br2 in graph:
      graph[br2].add(edge)
    else:
      graph[br2] = {edge}

  biorun_nodes = list(graph.keys())
  for biorun in biorun_nodes:
    biorun_edges = graph.get(biorun, {})
    unvisited_edge_list = list(biorun_edges)
    
    while len(unvisited_edge_list) > 0:
      traversed_edge = unvisited_edge_list.pop(0)
      biorun_edges.add(traversed_edge)

      traversed_node = traversed_edge.biorun_2

      # Add all Comparison-edges to be linked to @biorun and then mark biorun as "traversed"
      if traversed_node in graph:
        unvisited_edges = list(graph.get(traversed_node, {}))
        unvisited_edge_list.extend(unvisited_edges)
        del graph[traversed_node]

  samples_tsv_contents = [ ['sample', 'biorun_1', 'biorun_2', 'match_proportion'] ]
  sample_num = 0

  all_matched_bioruns_set = set()
  sample_to_bioruns_list = []
  
  for br, biorun_edges in graph.items():
    same_sample = set()
    for match in biorun_edges:
      br1 = match.biorun_1
      br2 = match.biorun_2

      same_sample.add(br1)
      same_sample.add(br2)
      all_matched_bioruns_set.add(br1)
      all_matched_bioruns_set.add(br2)

      sample_line = [ str(sample_num), br1, br2, str(match.match_proportion) ]
      samples_tsv_contents.append(sample_line)
    
    sample_to_bioruns_list.append(list(same_sample))
    sample_num += 1    

  return sample_num, samples_tsv_contents, sample_to_bioruns_list, all_matched_bioruns_set

def get_processed_vcf_list(vcf_list):
  '''Extract relevant VCF values from list of vcf files 
  :param vcf_list, str[] - List of VCF files
  :return {}
  '''
  num_vcfs = len(vcf_list)
  print("Processing VCF files...")
  processed_vcf_list = []

  all_bioruns = set()

  for idx, vcf in enumerate(vcf_list):
    if idx % 10 == 0:
      print("\tProcessed: %s/%s" % (idx, num_vcfs))
    biorun = get_biorun(vcf)
    all_bioruns.add(biorun)
    map = get_vcf_map(vcf, biorun)
    processed_vcf_list.append(map)

  return processed_vcf_list, all_bioruns

def get_comparisons(processed_vcf_list):
  '''Compares processed vcf list
  :param processed_vcf_list, dict[]
  :return Comparison[]
  '''
  print("Finding Matches...")

  num_vcfs = len(processed_vcf_list)
  intervals_ten_per = max(int(num_vcfs/10), 1)
  
  comparisons = []
  for i in range(num_vcfs):
    vcf_map1 = processed_vcf_list[i]
    for j in range(i+1, num_vcfs):
      vcf_map2 = processed_vcf_list[j]
      match_proportion = get_match_proportion(vcf_map1, vcf_map2)
      is_match = match_threshold <= match_proportion
      comparisons.append(Comparison(vcf_map1['biorun'], vcf_map2['biorun'], match_proportion, is_match))

    if (i % intervals_ten_per) == 0:
      print("\tCompared: %s/%s" % (i+1, num_vcfs))

  return comparisons

def write_all_sample_out_file(sample_bioruns_list, fname):
  with open(fname, 'w') as out:
    for idx, sample_bioruns in enumerate(sample_bioruns_list):
      sample_bioruns_str = '\t'.join(sample_bioruns)
      line = '%s\t%s\n' % (idx, sample_bioruns_str)
      out.write(line)

if __name__ == '__main__':
  match_threshold = float(sys.argv[1])
  vcf_list = sys.argv[2:]
  
  num_vcfs = len(vcf_list)
  out_file = 'comparisons___n%s___t%s.tsv' % (num_vcfs, match_threshold)
  sample_out_file = 'matched_samples___n%s___t%s.tsv' % (num_vcfs, match_threshold)
  all_sample_out_file = 'all_samples___n%s___t%s.tsv' % (num_vcfs, match_threshold)
  start = time.time()

  print("CONFIG")
  print("\tMIN_THRESHOLD=%s" % match_threshold)
  print("\tOUTPUT [compare]: %s" % out_file)
  print("\tOUTPUT [samples]: %s" % sample_out_file)
  print("\tNUM VCF FILES=%s" % num_vcfs)

  processed_vcf_list, biorun_set = get_processed_vcf_list(vcf_list)
  processing_done_time = time.time()
  print("\tProcessing Time (seconds) = %s" % (processing_done_time-start))

  comparisons = get_comparisons(processed_vcf_list)
  print("\tComparison Time (seconds) = %s" % (time.time()-processing_done_time))
  match_proportions = [ comp.match_proportion for comp in comparisons ]
  summ_stats(match_proportions)
  comparisons.sort(key=lambda comp: comp.match_proportion)
  log_comparisons(comparisons, out_file)

  match_list = [ comp for comp in comparisons if comp.is_match ]
  num_matched_samples, samples_tsv_contents, sample_to_bioruns_list, all_matched_bioruns_set = identify_same_samples(match_list)
  log_samples(samples_tsv_contents, sample_out_file)

  unmatched_bioruns_set = biorun_set.difference(all_matched_bioruns_set)
  unmatched_bioruns_list = [ [biorun] for biorun in list(unmatched_bioruns_set) ]

  sample_to_bioruns_list.extend(unmatched_bioruns_list)
  write_all_sample_out_file(sample_to_bioruns_list, all_sample_out_file)

  graph(match_proportions, 'match proportions num=%s' % num_vcfs)

  if len(WARNINGS) > 0:
    print("WARNINGS")
    for k,v in WARNINGS.items():
      print('\t%s (Affected bioruns: %s)' % (k, len(v)))

  end=time.time()

  num_unmatched_bioruns = len(unmatched_bioruns_list)
  total_samples = num_unmatched_bioruns + num_matched_samples

  print("SUMMARY")
  print('\tMatching Bioruns: %s' % len(list(all_matched_bioruns_set)))
  print('\tUnmatched Bioruns: %s' % num_unmatched_bioruns)
  print('\tSamples w/ Multiple Bioruns: %s' % num_matched_samples)
  print('\tTotal Samples: %s' % total_samples)
  print("TIME (seconds) = %s" % (end-start))