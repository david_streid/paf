#!/usr/bin/env python

import sys

from UpdateVyxPaf import Comparison, get_patient_to_bioruns_list, get_patient_biorun_map, create_bed_files

f = "/path/to/filtered_comparisons.tsv" # CREATED IN FIRST STEP

def parse_line(line):
  vals = line.strip().split('\t')
  bp1 = vals[0].split('___')
  bp2 = vals[1].split('___')
  match_proportion = 'n/a' if vals[2] == 'n/a' else float(vals[2])
  original_is_match = vals[3]
  if original_is_match == 'True':
    is_match = True
  else:
    is_match = match_proportion != 'n/a' and match_proportion >= 0.7
 
  biorun_1 = bp1[0]
  patient_1 = bp1[1]
  biorun_2 = bp2[0]
  patient_2 = bp2[1]

  if biorun_1 == biorun_2:
    is_match == False

  return Comparison(biorun_1, patient_1, biorun_2, patient_2, match_proportion, is_match)

vcf_list = sys.argv[1:]
patient_biorun_map = get_patient_biorun_map(vcf_list)

with open(f, 'r') as contents:
  comparisons = []
  print("Getting comparisons...")
  idx = 0
  for line in contents:
    idx += 1
    comparisons.append(parse_line(line))
    if idx % 1000000 == 0:
      print("\tCompared %s" % idx)
 
  print("Identifying patients") 
  sample_to_bioruns_list = get_patient_to_bioruns_list(comparisons, 'sample_list.tsv')
  create_bed_files(sample_to_bioruns_list, patient_biorun_map)
