# Converts the comparisons file to BED files

## Files needed
c_file - file of ocmparisons from pipeline
f - biorun-VCF header lines of (ideally) just the VCF files w/ multiple patients
LIST_OF_sv.gt.hg38.vcf - list of VCF files the pipeline ran on

# Get f - we need all the headers
```
grep CHROM $(cat ~/david-workspace/paf_work/vyx_internal_paf/biorun_panel_2374.txt) | cut -f10- > f
```

## Modify comparisons file to have True at threshold desired and w/ marking of repeat multiple-patient bioruns
```
# Modify f & c_file
python3 modify_comparisons_for_0.7.py > filtered_comparisons.tsv
```

## Run wrapper (calls `get_beds_for_0.7.py`)
```
./wrapper.sh &
```




