#!/usr/bin/env python

import sys

f = "/home/pplnuser/david-workspace/paf_work/vyx_internal_paf/comparisons_to_beds/all_old/remove_different/header_lines_multiple_patients.txt"
''' This file is a list of line-separated groupings of patients that were in a single biorun
e.g.
  202206763       202206772       202206657
  VYXV040010202   VYXV041010203   VYXV03900500P
  202221009       202220657       202220994
  ...
'''
c_file = "/home/pplnuser/david-workspace/paf_work/vyx_internal_paf/comparisons_to_beds/all_old/old/original_comparisons___n2374___t0.95.tsv"
''' This is the original comparisons file output by the script
e.g.
  biorun___patient_1      biorun___patient_2      match_proportion        is_match
  a0U7R00001Rv6fzUAB_BIA_21-10-27_40116___VYXV040010202   a0U7R00001Rsaz7UAB_BIA_21-08-31_38252___VYXV040010202   n/a     True
  ...
'''

def get_patients(line):
  vals = line.strip().split('\t')
  
  bp1 = vals[0].split('___')
  bp2 = vals[1].split('___')

  patient_1 = bp1[1]
  patient_2 = bp2[1]

  mp = vals[2]

  b1 = bp1[0]
  b2 = bp2[0]

  return patient_1, b1, patient_2, b2, mp

rp_map = {}
with open(f, 'r') as contents:
  comparisons = []
  # print("Getting comparisons...")
  for line in contents:
    patients = line.strip().split('\t')
    for idx in range(len(patients)):
      not_patient = patients.copy()
      not_patient.pop(idx)
      patient = patients[idx]
      if patient in rp_map:
        for rp in patients:
          rp_map[patient].update(not_patient)
      else:
        rp_map[patient] = set(not_patient)

def is_repeat_biorun(p1, p2, p1_other_set, p2_other_set):
  if p1 in p2_other_set or p2 in p1_other_set:
    return True
  b1_patients = p1_other_set.union(set(p1))
  b2_patients = p2_other_set.union(set(p2))
  
  if len(b1_patients.intersection(b2_patients)) > 0: # p2_other_set.intersection(p1_other_set)) > 0:
    return True
  return False

with open(c_file, 'r') as comparison_contents:
  for line in comparison_contents:
    if line.startswith('biorun___patient_1'):
      # skip the header
      continue
    p1, b1, p2, b2, mp = get_patients(line)
    if p1 == p2:
      print(line.strip())	# this should be right
    elif b1 == b2 and p1 != p2:
      no_match = vals[:3] + [ 'False' ]
      print('\t'.join(no_match))
    elif is_repeat_biorun(p1, p2, rp_map.get(p1, set()), rp_map.get(p2, set())): # p1 in rp_map and p2 in rp_map[p1] or p2 in rp_map and p1 in rp_map[p2]:
      # If either patient has been in the other's biorun, they can't be the same 
      vals = line.strip().split('\t')
      no_match = vals[:3] + [ 'False' ]
      print('\t'.join(no_match))
    elif mp != 'n/a' and float(mp) >= 0.7:
      vals = line.strip().split('\t')
      no_match = vals[:3] + [ 'True' ]
      print('\t'.join(no_match))      
    else:
      print(line.strip())

#for patient in rp_map:
#  print("%s => [ %s ]" % (patient, ' '.join(list(rp_map[patient]))))
