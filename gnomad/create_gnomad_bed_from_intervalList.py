#!/usr/bin/env python

import sys

UNLIFTED_TOTAL = 1129   # This should be in the LiftOverIntervalList logs, e.g. '1129 of 387477 intervals failed (0.291372%) to liftover'
SEPARATOR = '\t'
NOT_PROCESSED_SV_TYPES = { 'CN=0', 'INS', 'CPX', 'CTX' }
# 'CN=0', i.e. mCNVs - have at least 3 predicted alleles and are not processed for now
# 'INS' - More involved to process TODO
# 'CPX', i.e. multiple structural variants - need to be accounted for separately TODO
# 'CTX', i.e. translocation

BED_SV_TYPE_COL = 'svtype'
BED_SV_TYPE_DEL = 'DEL'
BED_SV_TYPE_BND = 'BND'
BED_SV_TYPE_DUP = 'DUP'
BED_SV_TYPE_INV = 'INV'
BED_SV_TYPE_INS_ME_ALU = 'INS:ME:ALU'
BED_SV_TYPE_INS_ME_LINE1 = 'INS:ME:LINE1'
BED_SV_TYPE_INS_ME_SVA = 'INS:ME:SVA'
BED_SV_TYPE_INS_ME = 'INS:ME'

STD_BED_COLS = [ 'chrom', 'start', 'end', 'name' ]
AF_BED_FILE_COLS = [
  BED_SV_TYPE_COL,
  'FILTER',
  'AN',
  'AC',
  'AF',
  'N_HOMREF', 
  'N_HET', 
  'N_HOMALT'
  # These don't seem super important
  # 'CHR2',
  # 'POS2',
  # 'END',
  # 'END2',
  # 'SOURCE',
  # 'PAR',
  # 'SVLEN',
  # 'UNRESOLVED_TYPE'

  # 'SVTYPE',             # Redundant
  # 'STRANDS',            # always 'NA' for [ DEL, INS:ME:ALU, BND, DUP, INS:ME:LINE1, INS:ME:SVA, INV, INS:ME ]
  # 'CPX_TYPE',           # always 'NA' for [ DEL, INS:ME:ALU, BND, DUP, INS:ME:LINE1, INS:ME:SVA, INV, INS:ME ]
  # 'CPX_INTERVALS',      # always 'NA' for [ DEL, INS:ME:ALU, BND, DUP, INS:ME:LINE1, INS:ME:SVA, INV, INS:ME ]
]

class Entry:
  def __init__(self, chr, start, end, id):
    self.chr = chr
    self.start = start
    self.end = end
    self.id = id

def get_iList_map(iList):
  iList_map = {}
  with open(iList, 'r') as iList_file:
    for line in iList_file:
      if line.startswith('@'):
        continue
      chr, start, end, plus, id = line.strip().split(SEPARATOR)
      iList_map[id] = Entry(chr, start, end, id)
  return iList_map

def get_bed_map(bed_file_name):  
  bed_map = {}
  with open(bed_file_name, 'r') as iList_file:
    for line in iList_file:
      if line.startswith('#'):
        headers = line.strip('\n').split(SEPARATOR)
        continue
      bed_col_line = dict(zip(headers, line.strip('\n').split(SEPARATOR)))
      id = bed_col_line['name']
      bed_map[id] = bed_col_line

  return bed_map

def get_header():
  std_bed_header_str = SEPARATOR.join(STD_BED_COLS)
  af_bed_header_str = SEPARATOR.join(AF_BED_FILE_COLS) 
  return '#%s\t%s\n' % (std_bed_header_str, af_bed_header_str)

def get_sv_type(entry):
  return entry[BED_SV_TYPE_COL]

def get_bf_names():
  del_bf = 'GNOMAD.sv.%s.paf.bed' % 'del'
  bnd_bf = 'GNOMAD.sv.%s.paf.bed' % 'bnd'
  mei_bf = 'GNOMAD.sv.%s.paf.bed' % 'mei'
  dup_bf = 'GNOMAD.sv.%s.paf.bed' % 'dup'
  inv_bf = 'GNOMAD.sv.%s.paf.bed' % 'inv'

  return del_bf, bnd_bf, mei_bf, dup_bf, inv_bf

def get_sv_type(bed_entry):
  return bed_entry[BED_SV_TYPE_COL]

def write_headers(file_handler_list):
  bed_header = get_header()
  for fh in file_handler_list:
    fh.write(bed_header)

def write_bed_files(bed_map, iList_map):
  del_bf_name, bnd_bf_name, mei_bf_name, dup_bf_name, inv_bf_name = get_bf_names()
  with open(del_bf_name, "w") as del_bf, \
    open(bnd_bf_name, "w") as bnd_bf, \
    open(mei_bf_name, "w") as mei_bf, \
    open(dup_bf_name, "w") as dup_bf, \
    open(inv_bf_name, "w") as inv_bf :

    write_headers([del_bf, bnd_bf, mei_bf, dup_bf, inv_bf])
    file_chooser = {
      BED_SV_TYPE_DEL: del_bf,
      BED_SV_TYPE_BND: bnd_bf,
      BED_SV_TYPE_INV: inv_bf,
      BED_SV_TYPE_DUP: dup_bf,
      BED_SV_TYPE_INS_ME_ALU: mei_bf,
      BED_SV_TYPE_INS_ME_LINE1: mei_bf,
      BED_SV_TYPE_INS_ME_SVA: mei_bf,
      BED_SV_TYPE_INS_ME: mei_bf
    }

    not_processed_svtypes = {}
    for vId in iList_map.keys():
      bed_entry = bed_map[vId]
      iList_entry = iList_map[vId]

      # We need to convert the 1-based interval start to 0-based for the BED file
      iList_vals = [ iList_entry.chr, str(int(iList_entry.start) - 1), iList_entry.end ]
      bed_vals_req = [ bed_entry[col] for col in AF_BED_FILE_COLS ]
      bed_line = SEPARATOR.join([SEPARATOR.join(iList_vals), vId, SEPARATOR.join(bed_vals_req)])

      sv_type = get_sv_type(bed_entry)
      f = file_chooser.get(sv_type, None)
      if f:
        f.write('%s\n' % bed_line)
      else:
        not_processed_svtypes[sv_type] = not_processed_svtypes.get(sv_type, 0) + 1
    
    if len(not_processed_svtypes) > 0:
      print("Did not process svtypes: [ %s ]" % ' '.join(list(not_processed_svtypes.keys())))
      for sv_type, ct in not_processed_svtypes.items():
        print("\t%s: %s" % (sv_type, ct))
      unexpected_types = set(not_processed_svtypes.keys()).difference(set(NOT_PROCESSED_SV_TYPES))
      if len(unexpected_types) > 0:
        print("[ERROR] Unexpecgted SV_TYPE was not processed: %s" % ' '.join(list(unexpected_types)))
        sys.exit(1)


def validate_iList_and_bed(iList_map, bed_map):
  iK = iList_map.keys()
  bK = bed_map.keys()

  iS = set(iK)
  bS = set(bK)

  lifted_variants = bS.intersection(iS)
  shared = len(lifted_variants)
  difference = len(bS.difference(iS))
  iList_total = len(iS)

  has_all = shared == iList_total
  expected_differences = difference == UNLIFTED_TOTAL

  if not has_all or not expected_differences:
    print("Unexpected inputs. has_all=%s expected_differences=%s" % (has_all, expected_differences))
    sys.exit(1)

if __name__ == '__main__':
  if len(sys.argv) != 3:
    print("Please provide a valid .interval_list and the original .bed file")
    print("python3 create_gnomad_bed_from_intervalList.py lifted_gnomad_v2.1_sv.sites.interval_list gnomad_v2.1_sv.sites.bed")
    sys.exit(1)

  iList_file_name = sys.argv[1]
  bed_file_name = sys.argv[2]

  iList_map = get_iList_map(iList_file_name)
  bed_map = get_bed_map(bed_file_name)

  validate_iList_and_bed(iList_map, bed_map)

  write_bed_files(bed_map, iList_map)
    
