#!/bin/bash

OUT_DIR=work
mkdir -p ${OUT_DIR}

BED=$1          # /Users/dstreid/bioinformatics_data/bed/gnomad_v2.1_sv.sites.bed
SOURCE_DICT=$2  # /Users/dstreid/work/sv_pop_freq/gnomad/gatkLiftOverIntervalList/supporting/GRCh37.dict
TARGET_DICT=$3  # /Users/dstreid/work/database_update/datafiles/reference/hg38/hg38.fa.dict # /Users/dstreid/work/sv_pop_freq/gnomad/gatkLiftOverIntervalList/supporting/hg38.dict
CHAIN=$4        # /Users/dstreid/bioinformatics_data/chain_files/hg19ToHg38.over.chain.gz
TARGET_REF=$5   # /Users/dstreid/work/database_update/datafiles/reference/hg38/hg38.fa

if [[ ! -f ${BED} || ! -f ${SOURCE_DICT} || ! -f ${TARGET_DICT} || ! -f ${CHAIN} || ! -f ${TARGET_REF} ]]; then
  echo "INVALID USAGE"
  printf "\tARG1=gnomad BED file\n"
  printf "\tARG2=seq-dict of source GNOMAD BED reference\n"
  printf "\tARG3=seq-dict of target reference\n"
  printf "\tARG4=*.chain.gz file of source to target\n"
  printf "\tARG5=Reference *.fa of target reference\n"
  echo "./liftover_gnomad_bed.sh gnomad_v2.1_sv.sites.bed GRCh37.dict hg38.fa.dict hg19ToHg38.over.chain.gz hg38.fa"
  exit 1
fi

no_dict_interval_list=${OUT_DIR}/gnomad_no_dict.chrom.interval_list

iList_unfiltered=${OUT_DIR}/gnomad_v2.1_sv.sites.interval_list
UNLIFTED_SOURCE_ILIST=${OUT_DIR}/gnomad_v2.1_sv_chr17clipped.sites.interval_list

echo "Creating interval_list=${no_dict_interval_list}"
# Write all but the header to the interval list
# 	e.g. 	"1       30366   30503   +       target_1"
#   Convert 0-based bed file to 1-based interval-list file
tail -n +2 ${BED} | cut -f1-4 | awk '{print "chr"$1"\t"$2+1"\t"$3"\t+\t"$4}' >> ${no_dict_interval_list}

echo "Adding sequence dict to interval_list=${iList_unfiltered}"
cat ${SOURCE_DICT} ${no_dict_interval_list} > ${iList_unfiltered} # gnomad_v2.1_sv.sites.iList

echo "Filtering out problematic regions to create source liftover_list=${UNLIFTED_SOURCE_ILIST}"
sed 's/81196000/81195210/' ${iList_unfiltered} > ${UNLIFTED_SOURCE_ILIST} # gnomad_v2.1_sv_chr17clipped.sites.iList

fname_basename=$(basename ${UNLIFTED_SOURCE_ILIST})
LOG=${OUT_DIR}/gatk_liftover.out
OUTPUT="lifted_${fname_basename}"
REJECT="unlifted_${fname_basename}"
echo "Running GATK Liftover - LOG=${LOG} LIFTED=${OUTPUT} REJECT=${REJECT}"
CMD="gatk LiftOverIntervalList I=${UNLIFTED_SOURCE_ILIST} O=${OUTPUT} SD=${TARGET_DICT} CHAIN=${CHAIN} REFERENCE_SEQUENCE=${TARGET_REF} REJECT=${REJECT} >> ${LOG} 2>&1"
echo ${CMD} > ${LOG} 
eval ${CMD}

echo "DONE."
echo "Created ${OUTPUT}"
