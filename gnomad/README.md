# gnomad population frequency parsing

## Steps
1) Download these files

* [gnomad SV 2.1 sites BED](https://gnomad.broadinstitute.org/downloads#v2-structural-variants)

* hg38 reference genome

2) Create hg38 dictionary file
```
java -jar picard.jar CreateSequenceDictionary \ 
      R=reference.fasta \ 
      O=reference.dict
```

3) Run the file creation scripts
```
./liftover_gnomad_bed.sh gnomad_v2.1_sv.sites.bed GRCh37.dict hg38.fa.dict hg19ToHg38.over.chain.gz hg38.fa
python3 create_gnomad_bed_from_intervalList.py lifted_gnomad_v2.1_sv.sites.interval_list gnomad_v2.1_sv.sites.bed
```

### End setup
```
.
├── GNOMAD.sv.bnd.paf.bed
├── GNOMAD.sv.del.paf.bed
├── GNOMAD.sv.dup.paf.bed
├── GNOMAD.sv.inv.paf.bed
├── GNOMAD.sv.mei.paf.bed
├── lifted_gnomad_v2.1_sv_chr17clipped.sites.interval_list
├── unlifted_gnomad_v2.1_sv_chr17clipped.sites.interval_list
└── work
    ├── gatk_liftover.out
    ├── gnomad_no_dict.chrom.interval_list
    ├── gnomad_v2.1_sv.sites.interval_list
    └── gnomad_v2.1_sv_chr17clipped.sites.interval_list
```
