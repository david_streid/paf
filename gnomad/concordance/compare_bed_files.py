import sys
import os

SINGLE_POSITION_EVENTS_INTERVAL = 40    # Twice StructuralVariationAnnotator::SINGLE_POSITION_EVENTS_INTERVAL
SINGLE_POSITION_EVENTS_OVERLAP = 0.01
OTHER_OVERLAP = 0.8

def percent_overlap(pos1, pos2):
  '''
    returns % of pos1 covered by pos2 and vice versa, i.e. 
    overlap((0,100), (50,1050)) -> 0.5, 0.05
  '''
  len1 = get_pos_len(pos1)
  len2 = get_pos_len(pos2)
  overlap_bases = get_overlap_bases(pos1, pos2)
  return overlap_bases / len1, overlap_bases / len2

def get_reciprocal_position_overlap(pos1, pos2):
  ''' Returns the minimum overlap between two regions 
  :param start1, stop1, start2, stop2; int
  '''
  len1 = get_pos_len(pos1)
  len2 = get_pos_len(pos2)
  overlap_bases = get_overlap_bases(pos1, pos2)

  return min(overlap_bases / len1, overlap_bases / len2) 

def overlap(pos1, pos2, perc=1):
  ''' Determines whether two positions have reciprocal overlap greater than @perc
  :param pos1, (int, int) - (reference) positions for structural variant
  :param pos2, (int, int) - (sample) posisions for structural variant
  :return Boolean
  '''
  if perc == 0:
    overlap_bases = get_overlap_bases(pos1, pos2)
    return overlap_bases > 0

  reciprocal_overlap = get_reciprocal_position_overlap(pos1, pos2)
  return reciprocal_overlap >= perc

def get_overlap_bases(pos1, pos2):
  start1, stop1 = pos1
  start2, stop2 = pos2
  return min(stop1, stop2) - max(start1, start2)

def get_pos_len(pos):
  ''' Unpacks @pos
  :param pos, (int, int)
  '''
  start, stop = pos
  len = stop - start
  return len

class CSE:
  def __init__(self, chr, str, end):
    self.chr = chr
    self.str = int(str)
    self.end = int(end)

  def get_overlap(self, chr, str, end):
    if self.chr != chr:
      print("[ERROR] Comparing chromosomes that aren't equal - source=%s target=%s" % (self.chr, chr))
    p1 = (self.str, self.end)
    p2 = (str, end)
    return get_reciprocal_position_overlap(p1, p2)

  def to_string(self):
    return "%s\t%s\t%s" % (self.chr, self.str, self.end)

def add_entry_to_map(map, chr, str, end):
  if chr not in map:
    map[chr] = []
  map[chr].append(CSE(chr, str, end))

def get_chr_map(b_fname, is_single_position_event = False):
  chr_map = {}
  with open(b_fname, 'r') as bed_contents:
    for line in bed_contents:
      if line.startswith('#chrom'):
        # gnomad BED file
        continue
      vals = line.split('\t')
      chr = vals[0]
      str = vals[1]
      end = vals[2]
      if is_single_position_event:
        str = max(int(str) - SINGLE_POSITION_EVENTS_INTERVAL, 1)
        end = max(int(end) + SINGLE_POSITION_EVENTS_INTERVAL, 1)

      add_entry_to_map(chr_map, chr, str, end)
  for cse_list in chr_map.values():
    cse_list.sort(key=lambda cse: cse.str)

  original = [cse.str for cse in chr_map['chr1'][500:523]]
  sorted_original = sorted(original)

  if original != sorted_original:
    print("ERROR")
    sys.exit(1)

  return chr_map

def has_overlap(source_cse_list, cse, overlap_match_threshold):
  for source_cse in source_cse_list:
    if cse.end < source_cse.str:
      break
    if source_cse.get_overlap(cse.chr, cse.str, cse.end) >= overlap_match_threshold:
      # print("QUERY\t%s" % cse.to_string())
      # print("SOURCE\t%s\t%s\t%s" % (source_cse.chr, source_cse.str, source_cse.end))
      # sys.exit(0)
      return True
  return False

def find_overlapping_bed_entries(source_bed_map, query_bed_map, is_single_position_event):
  overlap_match_threshold = SINGLE_POSITION_EVENTS_OVERLAP if is_single_position_event else OTHER_OVERLAP

  num_overlap = 0
  chr_list_len = [ len(cse_list) for cse_list in list(query_bed_map.values()) ]
  total = sum(chr_list_len)
  processed = 0
  ten_per_interval = int(total/10)
  for chr, cse_list in query_bed_map.items():
    processed += 1
    if processed % ten_per_interval == 0:
      print("OVERLAP=%s PROCESSED=%s" % (num_overlap, processed))
    source_cse_list = source_bed_map.get(chr, [])
    if len(source_cse_list) == 0:
      print("%s has no entries in source" % chr)
    for cse in cse_list:
      if has_overlap(source_cse_list, cse, overlap_match_threshold):
        num_overlap += 1
  return num_overlap, total

if __name__ == '__main__':
  if len(sys.argv) != 4:
    print("Please provide 2 bed files and specify the SV type")
    sys.exit(1)
  b1 = sys.argv[1]
  b2 = sys.argv[2]
  type = sys.argv[3]

  f1 = os.path.basename(b1)
  f2 = os.path.basename(b2)
  is_single_position_event = type == 'mei' or type == 'bnd'

  print("SOURCE=%s QUERY=%s" % (f1, f2))
  print("PARAMS")
  print("\tSPE_INTERVAL=%s SPE_OVERLAP=%s NON_SPE_OVERLAP=%s" % (SINGLE_POSITION_EVENTS_INTERVAL, SINGLE_POSITION_EVENTS_OVERLAP, OTHER_OVERLAP))

  b1_map = get_chr_map(b1, is_single_position_event)
  b2_map = get_chr_map(b2)

  overlapping, total = find_overlapping_bed_entries(b1_map, b2_map, is_single_position_event)

  print("REPORT")
  overlapping_percent = f"{float(overlapping/total):.5%}"
  print("\tOVERLAP=%s TOTAL=%s (%s)" % (overlapping, total, overlapping_percent))