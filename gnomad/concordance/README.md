
# Results

## Comparing sample TSVs (`compare_structural_variants.py`)

### Notes
#### At a minimum overlap of 0.8 for a VCF entry to match w/ gnomad
* **GNOMAD misses most VCF entries VYX captures**: ~75% structural variant annotations are excluded from gnomad
  * **Reducing minimum overlap has little effect**
    * Reducing minimum overlap required for gnomad by 10%,
      * Only increases the  matches by 313
      * Only increases VCF entries called by gnomad and VYX by 2%, i.e. 25% (at 0.8 overlap) to 27% (at 0.7 overlap)
    * **However, it does increase proportion of GNOMAD-VYX matches w/ close AF's** - This went from 44% (at 0.8 overlap) to 46% (at 0.7 overlap) to 51% (at 0.5 overlap). *I would expect this to stay the same*
* **Most AF differences are ok if one order of magnitude difference are allowed**, but not if 20% difference is the threshold, e.g. `0.7` & `0.3` are OK by magnitude, not by 0.2 difference    
* **GNOMAD reports higher allele frequency (~90%)**
* **GNOMAD has higher sample size** - GNOMAD has >=1071 AN in 95% of their structural variant calls

### Shared Matches are low 
#### 80% gnomad overlap required for match
* GNOMAD & VYX had matches in 25% (4166) structural variant entries
* Of the variants w/ matches in both, only ~44% of the vyx & gnomad AF's are within 20% of each other
```
python3 compare_structural_variants.py structural_variants.tsv
Processing /mnt/storage/primary/dga02c/a0U3O000003MzhOUAS_BIA_21-10-13_39727/structural_variants.tsv entries
TOTAL: 16551
GNOMAD_AND_VYX=4166 (0.2517068455078243) VYX_ONLY=8099 (0.4893359917829738) GNOMAD_ONLY=224 (0.013533925442571445) NO_MATCHES=4062 (0.24542323726663043)
@MAXIMUM_DIFFERENCE=0.05
	OK=772 NOT=3394 (0.18530964954392704)
@MAXIMUM_DIFFERENCE=0.1
	OK=1156 NOT=3010 (0.2774843975036006)
@MAXIMUM_DIFFERENCE=0.15
	OK=1502 NOT=2664 (0.36053768602976477)
@MAXIMUM_DIFFERENCE=0.2
	OK=1842 NOT=2324 (0.442150744119059)
@MAXIMUM_DIFFERENCE=0.25
	OK=2222 NOT=1944 (0.5333653384541527)
@MAXIMUM_DIFFERENCE=0.3
	OK=2508 NOT=1658 (0.6020163226116179)
@MAXIMUM_DIFFERENCE=0.35
	OK=2836 NOT=1330 (0.6807489198271723)
@MAXIMUM_DIFFERENCE=0.4
	OK=3117 NOT=1049 (0.7481997119539127)
@MAXIMUM_DIFFERENCE=0.45
	OK=3326 NOT=840 (0.7983677388382141)
@MAXIMUM_DIFFERENCE=0.5
	OK=3511 NOT=655 (0.842774843975036)
```

#### 70% gnomad overlap required for match
* GNOMAD & VYX had matches in 27% (4429) structural variant entries
* Of the variants w/ matches in both, only ~46% of the vyx & gnomad AF's are within 20% of each other
```
$ python3 /home/pplnuser/david-workspace/paf_work/gnomad/compare_structural_variants.py ../structural_variants.tsv
Processing ../structural_variants.tsv entries
TOTAL: 16551
GNOMAD_AND_VYX=4429 (27%)
	@MAX_AF_DIFF=0.05	20% valid	(OK=897, NOT=3532)
	@MAX_AF_DIFF=0.1	30% valid	(OK=1307, NOT=3122)
	@MAX_AF_DIFF=0.15	38% valid	(OK=1684, NOT=2745)
	@MAX_AF_DIFF=0.2	46% valid	(OK=2058, NOT=2371)
	@MAX_AF_DIFF=0.25	56% valid	(OK=2466, NOT=1963)
	@MAX_AF_DIFF=0.3	62% valid	(OK=2759, NOT=1670)
	@MAX_AF_DIFF=0.35	70% valid	(OK=3091, NOT=1338)
VYX_ONLY=7836 (47%)
GNOMAD_ONLY=274 (2%)
NO_MATCHES=4012 (24%)
```

#### 50% gnomad overlpa required for match
* GNOMAD & VYX had matches in 30% (4947) structural variant entries
* Of the variants w/ matches in both, 51% of the vyx & gnomad AF's are within 20% of each other
```
$ python3 ../../../compare_structural_variants.py ../structural_variants.tsv
Processing ../structural_variants.tsv entries
TOTAL: 16551
GNOMAD_AND_VYX=4947 (30%)
	@MAX_AF_DIFF=0.05	24% valid	(OK=1212, NOT=3735)
	@MAX_AF_DIFF=0.1	34% valid	(OK=1688, NOT=3259)
	@MAX_AF_DIFF=0.15	43% valid	(OK=2118, NOT=2829)
	@MAX_AF_DIFF=0.2	51% valid	(OK=2524, NOT=2423)
	@MAX_AF_DIFF=0.25	60% valid	(OK=2962, NOT=1985)
	@MAX_AF_DIFF=0.3	66% valid	(OK=3262, NOT=1685)
	@MAX_AF_DIFF=0.35	73% valid	(OK=3598, NOT=1349)s
VYX_ONLY=7318 (44%)
GNOMAD_ONLY=439 (3%)
NO_MATCHES=3847 (23%)
```

### AF DIFFERENCES
#### Most AF differences are ok if one order of magnitude difference is allowed 
##### 0.8 recirpcoal overlap
* 92% of matches are ok if one order of magnitude difference is allowed 
```
	@MAX_AF_MAGNITUDE_DIFF=2	17% valid	(OK=712, NOT=3454)
	@MAX_AF_MAGNITUDE_DIFF=5	62% valid	(OK=2581, NOT=1585)
	@MAX_AF_MAGNITUDE_DIFF=10	92% valid	(OK=3832, NOT=334)
```
NOTE - this stays fairly consistent across different reciprocal overlaps
##### 0.7 recirpcoal overlap
```
GNOMAD_AND_VYX=4429 (27%)
	@MAX_AF_MAGNITUDE_DIFF=2	17% valid	(OK=733, NOT=3696)
	@MAX_AF_MAGNITUDE_DIFF=5	61% valid	(OK=2698, NOT=1731)
	@MAX_AF_MAGNITUDE_DIFF=10	91% valid	(OK=4011, NOT=418)
```

##### 0.5 recirpcoal overlap
```
GNOMAD_AND_VYX=4947 (30%)
	@MAX_AF_MAGNITUDE_DIFF=2	16% valid	(OK=784, NOT=4163)
	@MAX_AF_MAGNITUDE_DIFF=5	59% valid	(OK=2899, NOT=2048)
	@MAX_AF_MAGNITUDE_DIFF=10	87% valid	(OK=4313, NOT=634)
```

#### GNOMAD AF's tend to be higher
* Most gnomad AF's are higher than VYX's (~90%)
```
$ cat both.tsv  | cut -f12 | wc -l
4167
$ cat both.tsv  | cut -f12 | grep "-" | wc -l
3712
```
Even higher when just looking at the "invalid" entries (~97%)
```
$ cat invalid_from_both_at_0.05.tsv  | cut -f12 | grep "-" | wc -l
3302
$ cat invalid_from_both_at_0.05.tsv  | cut -f12 |  wc -l
3395
```


### SV-Type Breakdown - Best at Breakpoints/Duplications, Worst at Inversions/Deletions
* "Best" at identifying breakpoints and duplications, but even then not the best
* MEI: ~76%, BP: ~55%, Dup: ~58%, Inv: 92%, Del: ~86%
```
$ for tsv in ${tsvs}; do echo "${tsv}" && cut -f6 ${tsv} | sort| uniq -c |sort -nr; done
./both.tsv
   2879 Deletion
    860 Mobile Element Insertion
    262 Break Point
    152 Duplication
     13 Inversion
      1 sv_type
./invalid_from_both_at_0.05.tsv
   2485 Deletion
    662 Mobile Element Insertion
    146 Break Point
     89 Duplication
     12 Inversion
      1 sv_type
./vyx.tsv
   3941 Deletion
   1725 Mobile Element Insertion
   1443 Break Point
    911 Duplication
     79 Inversion
      1 sv_type
./gnomad.tsv
    124 Deletion
     48 Duplication
     34 Break Point
     17 Mobile Element Insertion
      1 sv_type
      1 Inversion
./none.tsv
   1371 Mobile Element Insertion
   1337 Deletion
   1001 Duplication
    325 Break Point
     27 Inversion
      1 sv_type
      1 Loss of Heterozygosity
```

### Most Allele Counts of "Invalid" are >= 1071. Only ~150, or ~4%, have lower allele counts

### Majority have PASS VCF filters
```
$ cat invalid_from_both_at_0.05.tsv | cut -f10 | sort | uniq -c | sort -nr
   2956 PASS
    229 PCRPLUS_ENRICHED,LOW_CALL_RATE
    103 UNRESOLVED
     29 LOW_CALL_RATE
     24 UNSTABLE_AF_PCRMINUS
     15 UNRESOLVED,UNSTABLE_AF_PCRMINUS,LOW_CALL_RATE
     10 UNSTABLE_AF_PCRMINUS,LOW_CALL_RATE
     10 UNRESOLVED,UNSTABLE_AF_PCRMINUS
     10 UNRESOLVED,LOW_CALL_RATE
      8 UNRESOLVED,PCRPLUS_ENRICHED,LOW_CALL_RATE
      1 gnomad_filter
$ cat both.tsv | cut -f10 | sort | uniq -c | sort -nr
   3515 PASS
    248 PCRPLUS_ENRICHED,LOW_CALL_RATE
    148 UNRESOLVED
     68 LOW_CALL_RATE
     60 UNSTABLE_AF_PCRMINUS
     59 UNRESOLVED,UNSTABLE_AF_PCRMINUS
     32 UNRESOLVED,UNSTABLE_AF_PCRMINUS,LOW_CALL_RATE
     15 UNRESOLVED,LOW_CALL_RATE
     13 UNSTABLE_AF_PCRMINUS,LOW_CALL_RATE
      8 UNRESOLVED,PCRPLUS_ENRICHED,LOW_CALL_RATE
      1 gnomad_filter
```

### QUESTIONS 
* Why are so many variants (~75%) excluded from gnomad's annotations? Note - decreasing the required overlap for a match has little effect
* Why does gnomad report (>90% of the time) the allele frequency as HIGHER than vyx? 
* What is a reasonable amount of allele frequency difference to allow?

## Comparing VYX & GNOMAD BED files (`summarize_same.sh` & `find_similar.py`)
* At 0.95 overlap for del, dupl, & inv, `inv` is the only one w/ shared
* At any overlap for mei & bnd, none are shared