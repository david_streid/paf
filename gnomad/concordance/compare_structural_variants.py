import sys
import json
import matplotlib.pyplot as plt

SEPARATOR = '\t'

VYX_AND_GNOMAD = 1
VYX_ONLY = 2
GNOMAD_ONLY = 3
NO_ENTRY = 4

def get_header():
  return SEPARATOR.join([ 'chr', 'start', 'end', 'overlap', 'name', 'sv_type', 'vyx_freq', 'gnomad_af', 'gnomad_an', 'gnomad_filter', 'status', 'difference' ])

class Entry:
  def __init__(self, chr, start, end, name, sv_type, vyx_freq, max_freq, gnomad_af, gnomad_an, gnomad_filter, ids, afs, sos):
    self.chr = chr
    self.start = int(start)
    self.end = int(end)
    self.name = name
    self.sv_type = sv_type
    self.vyx_freq = float(vyx_freq)
    self.max_freq = max_freq
    self.gnomad_af = float(gnomad_af)
    self.gnomad_an = int(gnomad_an)
    self.gnomad_ids = ids
    self.gnomad_afs = afs
    self.gnomad_filter = gnomad_filter
    self.sample_overlaps = sos

  def has_freqs(self):
    return self.vyx_freq > 0 and self.gnomad_af > 0

  def get_status(self):
    if self.has_freqs():
      return VYX_AND_GNOMAD
    elif self.vyx_freq > 0:
      return VYX_ONLY
    elif self.gnomad_af > 0:
      return GNOMAD_ONLY
    return NO_ENTRY

  def is_high_vyx_paf(self, freq):
    return self.vyx_freq > freq

  def is_valid(self, max_magnitude):
    if self.has_freqs():
      vyx_diff, gnomad_diff = self.get_magnitude_differences()  
      if max(vyx_diff, gnomad_diff) <= max_magnitude:
        return True
      return False
    else:
      return False

  def get_magnitude_differences(self):
    return self.vyx_freq/self.gnomad_af, self.gnomad_af/self.vyx_freq

  def get_vyx_difference(self):
    vyx_difference = self.vyx_freq - self.gnomad_af
    return vyx_difference

  def get_gnomad_difference(self):
    gnomad_difference = self.gnomad_af - self.vyx_freq
    return gnomad_difference

  def to_line(self):
    status = self.get_status()
    if self.has_freqs():
      vyx_diff, gnomad_diff = self.get_magnitude_differences()
    else:
      vyx_diff = 'N/A'
    line = [ self.chr, self.start, self.end, self.sample_overlaps, self.name, self.sv_type, self.vyx_freq, self.gnomad_af, self.gnomad_an, self.gnomad_filter, status, vyx_diff ]
    return line
  
  def to_string(self):
    return SEPARATOR.join([ str(e) for e in self.to_line() ])

def parse_gnomad_sv_paf_list(gnomad_paf_list_str):
  gnomad_paf_list = json.loads(gnomad_paf_list_str)
  gnomad_table = gnomad_paf_list["references"]
  gnomad_table_headers = gnomad_table[0]      # ["id", "af", "an", "ac", "filter", "n_homref", "n_het", "n_homalt", "sample_overlap"]
  gnomad_table_rows = gnomad_table[1:]

  parsed = []
  for row in gnomad_table_rows:
    row_map = dict(zip(gnomad_table_headers, row))
    id = row_map['id']
    af = row_map['af']
    sample_overlap = row_map['sample_overlap']
    parsed.append([id, af, sample_overlap])

  if len(parsed) > 0:
    ids = ';'.join([ str(r[0]) for r in parsed ])
    afs = ';'.join([ str(r[1]) for r in parsed ])
    sos = ';'.join([ str(r[2]) for r in parsed ])

    primary_af = gnomad_table_rows[0][1] # index of af
    primary_an = gnomad_table_rows[0][2] # index of an
    primary_filter = gnomad_table_rows[0][4]# index of filter
    return primary_af, primary_an, primary_filter, ids, afs, sos

  return '0.0', '0', '', None, None, None

def process_tsv(tsv_fname):
  both_reported_entries = []
  vyx_reported_entries = []
  gnomad_reported_entries = []
  none_entries = []
  with open(tsv_fname, 'r') as tsv_contents:
    for line in tsv_contents:
      if line.startswith('chromosome__c'):
        print("Processing %s entries" % tsv_fname)
        headers = line.strip().split(SEPARATOR)
        continue
      vals = line.strip().split(SEPARATOR)
      line_map = dict(zip(headers, vals))
      chr = line_map['chromosome__c']
      start = line_map['start_coordinate__c']
      sv_type = line_map['structural_variant_type__c']
      end = line_map['stop_coordinate__c']
      name = line_map['name']
      vyx_freq = line_map['vyx_frequency__c']
      max_freq = line_map['max_frequency__c']
      gnomad_paf_list = line_map['gnomad_sv_paf_list__c']
      gnomad_af, gnomad_an, gnomad_filter, ids, afs, sos = parse_gnomad_sv_paf_list(gnomad_paf_list)

      entry = Entry(chr, start, end, name, sv_type, vyx_freq, max_freq, gnomad_af, gnomad_an, gnomad_filter, ids, afs, sos)

      status = entry.get_status()
      if status == VYX_AND_GNOMAD:
        both_reported_entries.append(entry)
      elif status == VYX_ONLY:
        vyx_reported_entries.append(entry)
      elif status == GNOMAD_ONLY:
        gnomad_reported_entries.append(entry)
      else:
        none_entries.append(entry)

  return both_reported_entries, vyx_reported_entries, gnomad_reported_entries, none_entries

def percent_overlap(pos1, pos2):
  '''
    returns % of pos1 covered by pos2 and vice versa, i.e. 
    overlap((0,100), (50,1050)) -> 0.5, 0.05
  '''
  len1 = get_pos_len(pos1)
  len2 = get_pos_len(pos2)
  overlap_bases = get_overlap_bases(pos1, pos2)
  return overlap_bases / len1, overlap_bases / len2


def get_overlap_bases(pos1, pos2):
  start1, stop1 = pos1
  start2, stop2 = pos2
  return min(stop1, stop2) - max(start1, start2)

def get_pos_len(pos):
  ''' Unpacks @pos
  :param pos, (int, int)
  '''
  start, stop = pos
  len = stop - start
  return len

def write_entries(entries, fname):
  lines = [ get_header() ]
  lines.extend([ entry.to_string() for entry in entries ])
  contents = '\n'.join(lines)
  f = open(fname, "w")
  f.write(contents)
  f.close()

def only_chars(num, num_chars):
  num_str = str(num)
  return num_str[0:num_chars]

def summ_stats(num_list, stat_name):
    min_of_list = min(num_list)
    max_of_list = only_chars(max(num_list), 20)
    len_of_list = len(num_list)
    median_of_list = only_chars(num_list[int(len_of_list/2)], 20)
    avg_of_list = only_chars(sum(num_list)/len_of_list, 20)
    list_size = only_chars(len_of_list, 20)

    print("\tavg=%s median=%s max=%s min=%s num=%s\t%s" % (avg_of_list, median_of_list, max_of_list, min_of_list, list_size, stat_name))

def print_lines(entry_list):
  print(get_header())
  for entry in entry_list:
    print(entry.to_string())

def analyze_differences(entries):
  both = [ entry for entry in entries if entry.get_status() == VYX_AND_GNOMAD ]
  vyx_only = [ entry for entry in entries if entry.get_status() == VYX_ONLY ]
  gnomad_only = [ entry for entry in entries if entry.get_status() == GNOMAD_ONLY ]
  no_matches = [ entry for entry in entries if entry.get_status() == NO_ENTRY ]


  vyx_only_vyx_freq = [ entry.vyx_freq for entry in vyx_only ]
  vyx_only_vyx_freq_gt_2 = [ entry.vyx_freq for entry in vyx_only if entry.vyx_freq > 0.02 ]
  summ_stats(vyx_only_vyx_freq_gt_2, 'vyx paf [only vyx, vyx_freq > 0.02]')

  vyx_only_vyx_freq_gt_15 = [ entry.vyx_freq for entry in vyx_only if entry.vyx_freq > 0.15 ]
  summ_stats(vyx_only_vyx_freq_gt_15, 'vyx paf [only vyx, vyx_freq > 0.15]')
  gnomad_only_gnomad_freq = [ entry.gnomad_af for entry in gnomad_only ]
  summ_stats(gnomad_only_gnomad_freq, 'gnomad paf [only gnomad]')

  num_total = len(entries)
  num_both = len(both)
  num_vyx_only = len(vyx_only)
  num_gnomad_only = len(gnomad_only)
  num_no_matches = len(no_matches)

  proportion_both = f"{float(num_both/num_total):.0%}"
  proportion_vyx_only = f"{float(num_vyx_only/num_total):.0%}"
  proportion_gnomad_only = f"{float(num_gnomad_only/num_total):.0%}"
  proportion_no_matches = f"{float(num_no_matches/num_total):.0%}"

  print("TOTAL: %s" % num_total)
  print("GNOMAD_AND_VYX=%s (%s)" % (num_both, proportion_both))

  caught_entries = [ entry for entry in both if entry.gnomad_af <= 0.02 and entry.vyx_freq > 0.02 ]
  missing_entries = [ entry for entry in both if entry.gnomad_af > 0.02 and entry.vyx_freq <= 0.02 ]

  summ_stats([ entry.vyx_freq for entry in missing_entries], 'vyx_af [entry.gnomad_af > 0.02, entry.vyx_freq <= 0.02]')
  summ_stats([ entry.vyx_freq for entry in caught_entries], 'vyx_af [entry.gnomad_af <= 0.02, entry.vyx_freq > 0.02]')

  write_entries(caught_entries, 'gnomad_rare_caught.tsv')
  write_entries(missing_entries, 'gnomad_rare_misses.tsv')

  # sv_types = list(set([ entry.sv_type for entry in both ]))
  valid_max_magnitudes = [ 10 ] 
  for magnitude in valid_max_magnitudes:
    valid_entries = [ entry for entry in both if entry.is_valid(magnitude) ]
    invalid_entries = [ entry for entry in both if not entry.is_valid(magnitude) ]
    num_true = len(valid_entries)
    num_false = len(invalid_entries)

    invalid_entries.sort(key=lambda entry: entry.vyx_freq)

    invalid_lt_15 = [ entry for entry in invalid_entries if entry.vyx_freq <= 0.15]
    invalid_lt_15_not_chrX = [ entry for entry in invalid_lt_15 if entry.chr  != 'chrX']

    summ_stats([ entry.vyx_freq for entry in invalid_lt_15 ], 'vyx_af [invalid, vyx_freq <= 0.15]')

    invalid_lt_15_gnomad_freqs = [ entry.gnomad_af for entry in invalid_lt_15 ]
    invalid_lt_15_not_chrX_gnomad_freqs = [ entry.gnomad_af for entry in invalid_lt_15_not_chrX ]
    summ_stats(invalid_lt_15_gnomad_freqs, 'gnoamd_af [invalid, vyx_freq <= 0.15]')
    summ_stats(invalid_lt_15_not_chrX_gnomad_freqs, 'gnoamd_af [invalid, vyx_freq <= 0.15, not chrX]')

    invalid_entries_vyx_freqs = [ entry.vyx_freq for entry in invalid_lt_15_not_chrX ]
    summ_stats(invalid_entries_vyx_freqs, 'vyx_af [invalid, vyx_freq <= 0.15, not chrX]')

    vyx_freq_diff_when_lt_15 = [ entry.get_vyx_difference() for entry in invalid_lt_15_not_chrX ]
    summ_stats(vyx_freq_diff_when_lt_15, '(vyx_af - gnomad_af) [invalid, vyx_freq <= 0.15, not chrX]')

    invalid_lt_15_not_chrX_mag_diffs = [entry.get_magnitude_differences()[0] for entry in invalid_lt_15_not_chrX]
    summ_stats(invalid_lt_15_not_chrX_mag_diffs, '(vyx_af/gnomad_af) [invalid, vyx_freq <= 0.15, not chrX]')

    invalid_lt_15_not_chrX_gnomad_gt = [entry for entry in invalid_lt_15_not_chrX if entry.get_gnomad_difference() >= 0]
    invalid_lt_15_not_chrX_gnomad_gt_mag_diffs = [entry.get_magnitude_differences()[1] for entry in invalid_lt_15_not_chrX_gnomad_gt]
    summ_stats(invalid_lt_15_not_chrX_gnomad_gt_mag_diffs, '(gnomad_af/vyx) [invalid, vyx_freq <= 0.15, not chrX, gnomad_af >= vyx_af]')
    graph(invalid_lt_15_not_chrX_gnomad_gt, 'invalid entries when lt 0.15 no chrX gnomad_af gt vyx_af')


    invalid_lt_15_not_chrX_gnomad_lt = [entry for entry in invalid_lt_15_not_chrX if entry.get_gnomad_difference() < 0]
    invalid_lt_15_not_chrX_gnomad_af = [entry.gnomad_af for entry in invalid_lt_15_not_chrX_gnomad_lt]
    summ_stats(invalid_lt_15_not_chrX_gnomad_af, 'gnomad_af [invalid, vyx_freq <= 0.15, not chrX, gnomad_af < vyx_af]')
    graph(invalid_lt_15_not_chrX_gnomad_lt, 'invalid entries when lt 0.15 no chrX gnomad_af lt vyx_af')

    invalid_lt_15_not_chrX_gnomad_lt_gnomad_mag_large = [ entry for entry in invalid_lt_15_not_chrX_gnomad_lt if entry.get_gnomad_difference() < -0.09 ]
    # print_lines(invalid_lt_15_not_chrX_gnomad_lt_gnomad_mag_large)

    invalid_lt_15_not_chrX_gnomad_reports_missing = [entry for entry in invalid_lt_15_not_chrX if entry.gnomad_af <= 0.02 and entry.vyx_freq > 0.02 ]
    summ_stats([ entry.gnomad_af for entry in invalid_lt_15_not_chrX_gnomad_reports_missing], 'gnomad_af [invalid, vyx_freq <= 0.15, not chrX, entry.gnomad_af <= 0.02, entry.vyx_freq > 0.02]')

    invalid_lt_15_not_chrX_gnomad_misses = [entry for entry in invalid_lt_15_not_chrX if entry.gnomad_af > 0.02 and entry.vyx_freq <= 0.02 ]
    summ_stats([ entry.gnomad_af for entry in invalid_lt_15_not_chrX_gnomad_misses], 'gnomad_af [invalid, vyx_freq <= 0.15, not chrX, entry.gnomad_af > 0.02, entry.vyx_freq <= 0.02]')
    write_entries(invalid_lt_15_not_chrX_gnomad_misses, 'gnomad_misses')

    graph(invalid_lt_15_not_chrX, 'invalid entries when lt 0.15 no chrX')
    write_entries(invalid_entries, 'invalid_from_both_at_%s.tsv' % magnitude)
    write_entries(valid_entries, 'valid_from_both_at_%s.tsv' % magnitude)
    graph(invalid_entries, 'MAG diff gt %s' % magnitude)

    proportion_ok = f"{float(num_true/num_both):.0%}"
    print("\t@MAX_AF_MAGNITUDE_DIFF=%s\t%s valid\t(OK=%s, NOT=%s)" % (magnitude, proportion_ok, num_true, num_false))


  for high_freq in [ 0, 0.15 ]: # [ 0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35 ]:
    high_paf_entries = [ entry for entry in both if entry.is_high_vyx_paf(high_freq) ]
    low_paf_entries = [ entry for entry in both if not entry.is_high_vyx_paf(high_freq) ]

    # chrX is incorrectly reported (I think?) - High PAF on chrX can have AC > AN
    # high_paf_entries_nonX = [ entry for entry in high_paf_entries if entry.chr != 'chrX' ]
    # high_paf_entries_X = [ entry for entry in high_paf_entries if entry.chr == 'X' ]

    # analyze_af_differences(high_paf_entries_nonX, high_freq)
    # analyze_af_differences(high_paf_entries_X, high_freq)
    analyze_af_differences(low_paf_entries, high_freq, 'lt')
    analyze_af_differences(high_paf_entries, high_freq, 'gt')

  print("VYX_ONLY=%s (%s)\nGNOMAD_ONLY=%s (%s)\nNO_MATCHES=%s (%s)" %
    (num_vyx_only, proportion_vyx_only, num_gnomad_only, proportion_gnomad_only, num_no_matches, proportion_no_matches))

def analyze_af_differences(entries, freq_filter, type):
  if len(entries) == 0:
    return
  entries_vyx_diff = [ entry.get_vyx_difference() for entry in entries ]
  entries_positive_vyx_diff = [ entry for entry in entries if entry.get_vyx_difference() > 0 ]

  graph(entries, 'AF Diff when VYX AF %s %s' % (type, freq_filter))
  graph(entries_positive_vyx_diff, 'VYX AF %s %s only pos vyx diff' % (type, freq_filter))
  write_entries(entries_positive_vyx_diff, 'entries_positive_vyx_diff_af_%s_%s.tsv' % (type, freq_filter))

  num_at_freq = len(entries)
  avg = sum(entries_vyx_diff)/num_at_freq
  print("\t@VYX_AF>%s\tNUM=%s\tAVG_DIFF=%s)" % (freq_filter, num_at_freq, avg))

  vyx_greater_prop = f"{float(len(entries_positive_vyx_diff)/len(entries)):.0%}"
  print("\t\tPROPORTION OF POSITIVE_VYX_DIFF=%s" % (vyx_greater_prop))
  write_entries(entries, 'vyx_paf_%s_%s_entries.tsv' % (type, freq_filter))

def graph(entry_list, title):
  vyx_differences = [ entry.get_vyx_difference() for entry in entry_list ]
  vyx_freqs = [ entry.vyx_freq for entry in entry_list ]
  plt.scatter(vyx_freqs, vyx_differences, color='#1f77b4') 
  plt.title(title)
  plt.xlabel('AF (vyx)')                    
  plt.ylabel('AF diff (vyx - gnomad)')   
  fname = "_".join(title.split(" "))
  plt.savefig("vyx_%s.pdf" % fname)
  plt.close()

  gnomad_freqs = [ entry.gnomad_af for entry in entry_list ]
  gnomad_differences = [ entry.get_gnomad_difference() for entry in entry_list ]
  plt.scatter(gnomad_freqs, gnomad_differences, color='#1f77b4') 
  plt.title(title)
  plt.xlabel('AF (gnomad)')                    
  plt.ylabel('AF diff (gnomad - vyx)')   
  fname = "_".join(title.split(" "))
  plt.savefig("gnomad_%s.pdf" % fname)
  plt.close()


if __name__ == '__main__':
  if len(sys.argv) != 2:
    print("Please provide the *.tsv file")
    sys.exit(1)

  tsv = sys.argv[1]

  both_reported_entries, vyx_reported_entries, gnomad_reported_entries, none_entries = process_tsv(tsv)
  write_entries(both_reported_entries, 'both.tsv')
  write_entries(vyx_reported_entries, 'vyx.tsv')
  write_entries(gnomad_reported_entries, 'gnomad.tsv')
  write_entries(none_entries, 'none.tsv')

  analyze_differences(both_reported_entries + vyx_reported_entries + gnomad_reported_entries + none_entries)






