for sv in bnd del dup inv mei; do 
  echo ${sv}
  saved=${sv}_shared.out
  python3 find_similar.py ../bed_files/GNOMAD.sv.${sv}.paf.bed ../bed_files/VYX.sv.${sv}.paf.bed ${sv} > ${saved}
  wc -l ${saved}
done
