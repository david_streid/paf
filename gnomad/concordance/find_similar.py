import sys

SEPARATOR = '\t'
MIN_OVERLAP = 0.95
MEI_BND_OVERLAP = 0.00

class Entry:
  def __init__(self, chr, start, end, id):
    self.chr = chr
    self.start = int(start)
    self.end = int(end)
    self.id = id

def process_bed(bf):
  bed_map = {}
  with open(bf, 'r') as bed_file:
    for line in bed_file:
      if line.startswith('#'):
        continue
      vals = line.strip().split(SEPARATOR)
      chr = vals[0]
      start = vals[1]
      end = vals[2]
      id = vals[3]
      bed_map[id] = Entry(chr, start, end, id)
  return bed_map

def percent_overlap(pos1, pos2):
  '''
    returns % of pos1 covered by pos2 and vice versa, i.e. 
    overlap((0,100), (50,1050)) -> 0.5, 0.05
  '''
  len1 = get_pos_len(pos1)
  len2 = get_pos_len(pos2)
  overlap_bases = get_overlap_bases(pos1, pos2)
  return overlap_bases / len1, overlap_bases / len2

def get_reciprocal_position_overlap(pos1, pos2):
  ''' Returns the minimum overlap between two regions 
  :param start1, stop1, start2, stop2; int
  '''
  len1 = get_pos_len(pos1)
  len2 = get_pos_len(pos2)
  overlap_bases = get_overlap_bases(pos1, pos2)

  return min(overlap_bases / len1, overlap_bases / len2) 

def has_overlap(pos1, pos2, perc=1):
  ''' Determines whether two positions have reciprocal overlap greater than @perc
  :param pos1, (int, int) - (reference) positions for structural variant
  :param pos2, (int, int) - (sample) posisions for structural variant
  :return Boolean
  '''
  if perc == 0:
    overlap_bases = get_overlap_bases(pos1, pos2)
    return overlap_bases > 0

  reciprocal_overlap = get_reciprocal_position_overlap(pos1, pos2)
  return reciprocal_overlap >= perc

def get_overlap_bases(pos1, pos2):
  start1, stop1 = pos1
  start2, stop2 = pos2
  return min(stop1, stop2) - max(start1, start2)

def get_pos_len(pos):
  ''' Unpacks @pos
  :param pos, (int, int)
  '''
  start, stop = pos
  len = stop - start
  return len

def compare_beds(b1_map, b2_map, sv_type):
  for b1Id, b1Entry in b1_map.items():
    for b2Id, b2Entry in b2_map.items():
      if sv_type in ['mei', 'bnd']:
        overlap = MEI_BND_OVERLAP
      else:
        overlap = MIN_OVERLAP
      if has_overlap((b1Entry.start, b1Entry.end), (b2Entry.start, b2Entry.end), overlap):
        print((b1Entry.start, b1Entry.end), (b2Entry.start, b2Entry.end))


if __name__ == '__main__':
  if len(sys.argv) != 4:
    print("Please two bed files to compare and the type")
    sys.exit(1)

  b1 = sys.argv[1]
  b2 = sys.argv[2]
  sv = sys.argv[3]

  b1_map = process_bed(b1)
  b2_map = process_bed(b2)

  compare_beds(b1_map, b2_map, sv)




