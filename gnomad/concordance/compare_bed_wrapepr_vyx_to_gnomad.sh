#!/bin/bash

for type in bnd del dup inv mei; do 
  echo "" > bed_compare_vyx_to_gnomad_${type}.out
done

for type in bnd del dup inv mei; do 
  echo "Processing ${type}"
  gnomad="path/to/GNOMAD.sv.${type}.paf.bed"
  vyx="path/to/VYX.sv.${type}.paf.bed"
  CMD="python3 compare_bed_files.py ${vyx} ${gnomad} ${type} > bed_compare_vyx_to_gnomad_${type}.out"
  echo ${CMD}
  eval ${CMD}
done
