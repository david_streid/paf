#!/bin/bash

for type in bnd del dup inv mei; do 
  echo "" > bed_compare_${type}.out
done

for type in bnd del dup inv mei; do 
  echo "Processing ${type}"
  gnomad="path/to/GNOMAD.sv.${type}.paf.bed"
  vyx="path/to/VYX.sv.${type}.paf.bed"
  CMD="python3 compare_bed_files.py ${gnomad} ${vyx} ${type} > bed_compare_${type}.out"
  echo ${CMD}
  eval ${CMD}
done
